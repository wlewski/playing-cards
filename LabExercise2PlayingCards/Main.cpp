/*
 -Enumeration - finite set of values
   - similar to classes where you can use it as a datatype once created
   - When running a code with enumerations in it, the names will be replaced with integers
   - Structs - how you would group code
   - Can be used in C# 
   - Similar to Classes
   - if you are trying to group data use a struct, if you're trying to store methods use a class


*/

// Lab Exercise 2 Playing Cards //
#include <iostream>
#include <conio.h>

using namespace std;

// set of values
enum Rank
{
	TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

enum Suit
{
	SPADE, HEART, CLUB, DIAMOND
};

// Stores data
struct Card
{
	Rank rank;
	Suit suit;
};

void Print(Card card);
Card HighCard(Card card1, Card card2);

int main()
{
	Card card1;
	card1.rank = TWO;
	card1.suit = SPADE;

	Card card2;
	card2.rank = TWO;
	card2.suit = HEART;

	Print(HighCard(card1, card2));

	_getch();
	return 0;
}

void Print(Card card) 
{
	cout << "The ";

	switch (card.rank)
	{
		case TWO: cout << "two"; break;
		case THREE: cout << "three"; break;
		case FOUR: cout << "four"; break;
		case 5: cout << "five"; break;
		case 6: cout << "six"; break;
		case 7: cout << "seven"; break;
		case 8: cout << "eight"; break;
		case 9: cout << "nine"; break;
		case 10: cout << "ten"; break;
		case 11: cout << "jack"; break;
		case 12: cout << "queen"; break;
		case 13: cout << "king"; break;
		case 14: cout << "ace"; break;
	}

	cout << " of ";

	switch (card.suit)
	{
		case SPADE: cout << "spades"; break;
		case HEART: cout << "hearts"; break;
		case CLUB: cout << "clubs"; break;
		case DIAMOND: cout << "diamonds"; break;
	}

	
}

// todo: discuss with team
Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank) return card1;
	else if (card1.rank < card2.rank) return card2;
	else if (card1.rank == card2.rank) {
		if (card1.suit > card2.suit) return card1;
		else if (card1.suit < card2.suit) return card2;
	}
}